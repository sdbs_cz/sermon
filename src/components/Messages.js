import React from 'react'

export default function Messages(props) {

    const formatDate = (timestamp) => {
        const d = new Date(timestamp * 1000)
        
        const day = (d.getDay() < 10 && `0`) + d.getDay()
        const month = ((d.getMonth() + 1) < 10 && `0`) + d.getMonth()
        const year = d.getFullYear()
        const hours = (d.getHours() < 10 && `0`) + d.getHours()
        const minutes = (d.getMinutes() < 10 && `0`) + d.getMinutes()

        return `${day}/${month}/${year} ${hours}:${minutes}`
    }

    const nameToRGB = (str) => {

        let hash = 0
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash)
        }

        const c = (hash & 0x00FFFFFF).toString(16).toUpperCase()
    
        return `#` + (`00000`.substring(0, 6 - c.length) + c)
    }

    return (
        <div>
            {props.messages.map(item =>
                <div
                    className={`message`}
                    key={item.id}
                >
                    <div className={`message__info`}>
                        <div className={`message__info-name`} style={{ color: nameToRGB(item.name) }}>
                            {item.name}
                        </div>
                        <div className={`message__info-datetime`}>[{formatDate(item.timestamp)}]</div>
                    </div>
                    <div className={`message__text`} dangerouslySetInnerHTML={{__html: item.text}}>
                    </div>
                </div>
            )}
        </div>
    )

}
