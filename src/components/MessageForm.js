import React from 'react'

export default function MessageForm(props) {

    return (
        <form
            method={`post`}
            className={`form form--message`}
        >

            <textarea
                name={`text`}
                className={`form__input-message`}
                placeholder={`message`}
                value={props.message}
                onChange={e => props.messageChange(e.target.value)}
            >
            </textarea>

            <input
                type={`submit`}
                value={`↵`}
                className={`form__input-submit`}
                onClick={e => {
                    e.preventDefault()
                    props.messageSubmit(e)
                }}
            />
        
        </form>
    )

}
