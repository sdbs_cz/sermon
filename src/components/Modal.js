import React from 'react'

export default function Modal(props) {

    const handleUsernameChange = (e) => {
        props.usernameChange(e.target.value)
    }

    const handleUsernameSubmit = (e) => {
        e.preventDefault()
        props.usernameSubmit()
    }

    return (
        <div className={`modal`}>
            <form method={`post`} className={`form form--modal`}>
                
                <input
                    type={`text`}
                    name={`name`}
                    placeholder={`enter your nickname`}
                    minLength={`3`}
                    maxLength={`20`}
                    className={`form__input-message`}
                    onChange={e => handleUsernameChange(e)}
                />

                <input
                    type={`submit`}
                    value={`↵`}
                    className={`form__input-submit`}
                    onClick={e => handleUsernameSubmit(e)}
                />

            </form>
        </div>
    )

}
