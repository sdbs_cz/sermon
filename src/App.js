import './App.css'

import React, { useState, useEffect } from 'react'

import Modal from './components/Modal'
import MessageForm from './components/MessageForm'
import Messages from './components/Messages'

import useInterval from './helpers/useInterval'


export default function App(props) {

    const [username, setUsername] = useState(``)

    const [modalActive, setModalActive] = useState(true)

    const [rooms, setRooms] = useState([])
    const [roomActive, setRoomActive] = useState(0)

    const [message, setMessage] = useState(``)

    const [messages, setMessages] = useState([])

    const [chatScrollStatus, setChatScrollStatus] = useState(`bottom`)

    /*
    ###
    ### USERNAME (MODAL)
    ###
    */

    /* username change */
    const handleUsernameChange = (username) => {
        setUsername(username)
    }

    /* username submit */
    const handleUsernameSubmit = () => {
        if(username.length >= 3) {
            localStorage.setItem(`sdbs-sermon-username`, username)
            setModalActive(false)
        }
    }

    /* submit username, write into local storage and close modal */
    useEffect(() => {
        if(localStorage.getItem(`sdbs-sermon-username`)) {
            setUsername(localStorage.getItem(`sdbs-sermon-username`))
            setModalActive(false)
        }
    }, [username, modalActive])


    /*
    ###
    ### ROOMS
    ###
    */
    const fetchRooms = (roomActive) => {
        fetch(`${process.env.REACT_APP_API_URL}?action=getRooms`)
            .then(response => response.json())
            .then(data => {
                if (!roomActive) {
                    setRoomActive(data[0].id)
                }
                setRooms(data)
            })
    }

    useEffect(() => {
        fetchRooms()
    }, [])

    useInterval(() => {
        fetchRooms(roomActive)
    }, 10000)

    const handleRoomChange = (id) => {
        setRoomActive(id)
        setChatScrollStatus(`bottom`)
    }
    
    
    /*
    ###
    ### CHAT ROOM SCROLLING
    ###
    */
    
    const handleChatScrollStatus = (chat) => {
        if((chat.scrollTop + chat.offsetHeight) < (chat.scrollHeight - 50)) {
            setChatScrollStatus(`scrolling`)
        } else {
            setChatScrollStatus(`bottom`)
        }
    }

    const handleChatScroll = () => {
        const chats = document.querySelectorAll(`.chat__room`)

        if(chatScrollStatus === `bottom`) {
            chats.forEach(item => {
                item.scrollTop = item.scrollHeight
            })
        }
    }


    /*
    ###
    ### MESSAGES
    ###
    */

    useEffect(() => {
        handleChatScroll()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [roomActive])

    const fetchMessages = () => {
        fetch(`${process.env.REACT_APP_API_URL}?action=getMessages`)
            .then(response => response.json())
            .then(data => {
                setMessages(data)
                handleChatScroll()
            })
    }

    useEffect(() => {
        fetchMessages()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useInterval(() => {
        fetchMessages()
    }, 5000)


    /*
    ###
    ### MESSAGE FORM
    ###
    */

    /* message change */
    const handleMessageChange = (message) => {
        setMessage(message)
    }

    /* message change */
    const handleMessageSubmit = (e) => {
        e.preventDefault()

        if(message.length > 0) {
            fetch(`${process.env.REACT_APP_API_URL}?action=createMessage`, {
                method: `POST`,
                headers: {
                    'Content-Type': `application/x-www-form-urlencoded`,
                },
                body: `name=${username}&text=${message}&room_id=${roomActive}`,
            }).then(response => {
                console.log(response)
                setMessage(``)
                fetchMessages()
            }).catch(error => {
                console.log(error)
            })
        }
    }


    /* ### RENDER ### */

    return (
        <div className={`wrapper`}>
            {modalActive && <Modal usernameChange={handleUsernameChange} usernameSubmit={handleUsernameSubmit} />}

            {rooms.length > 1 &&
                <div className={`room-list`}>
                    {rooms.map(room =>
                        <div
                            className={`room-list__item ${(roomActive === room.id) && `is-active`}`}
                            key={room.id}
                            title={room.name}
                            onClick={e => handleRoomChange(room.id)}
                        >
                            {room.name}
                        </div>
                    )}
                </div>
            }

            <div className={rooms.length <= 1 ? `chat chat--large` : `chat`}>
                {rooms.map(room =>
                    <div
                        className={`chat__room ${(roomActive === room.id) && `is-active`}`}
                        key={room.id}
                        onScroll={e => handleChatScrollStatus(e.target)}
                        data-id={room.id}
                    >
                        <Messages messages={messages.filter(m => m.room_id === room.id)} />
                    </div>
                )}
            </div>

            <MessageForm messageChange={handleMessageChange} messageSubmit={handleMessageSubmit} message={message} />
        </div>
    )

}
