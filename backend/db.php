<?php

try {
    $database = new SQLite3(BASE_PATH . '/db/sermon.db');
} catch (Exception $exception) {
    http_response_code(500);
    echo $exception;
    exit;
}

if (!file_exists(BASE_PATH . '/db/sermon.db')) {
    $database->exec(file_get_contents(BASE_PATH . '/db/sermon-ddl.sql'));
}

?>