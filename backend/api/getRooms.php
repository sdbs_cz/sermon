<?php

$statement = $database->prepare('
    SELECT *
    FROM rooms
    WHERE is_active = 1
')->execute();

$rooms = [];
while ($row = $statement->fetchArray(SQLITE3_ASSOC)) {
    $rooms[] = $row;
}

echo json_encode($rooms);

?>