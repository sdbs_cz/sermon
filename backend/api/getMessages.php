<?php

$statement = $database->prepare('
    SELECT messages.id, messages.room_id, messages.name, messages.text, messages.timestamp
    FROM messages
    JOIN rooms ON messages.room_id = rooms.id
    WHERE rooms.is_active = 1
')->execute();

$messageArray = [];
while ($row = $statement->fetchArray(SQLITE3_ASSOC)) {
    $row['datetime'] = date('d/m/Y H:i', $row['timestamp']);

    $image_search = preg_match('/(http|https):\/\/[^ ]+(\.gif|\.jpg|\.jpeg|\.png)/', $row['text'], $out);

    if ($image_search > 0) {
        $row['text'] = str_replace($out[0], '<p><img src="' . $out[0] . '" /></p>', $row['text']);
    } else {
        $row['text'] = $row['text'];
    }

    $messageArray[] = $row;
}

echo json_encode($messageArray);

?>