<?php

$timestamp = time();
$name = $database->escapeString(htmlspecialchars($_POST['name']));
$text = $database->escapeString(htmlspecialchars($_POST['text']));
$room_id = intval($database->escapeString(htmlspecialchars($_POST['room_id'])));

$statement = $database->prepare('INSERT INTO messages (name, text, timestamp, room_id) VALUES (:name, :text, :timestamp, :room_id)');

$statement->bindValue(':name', $name, SQLITE3_TEXT);
$statement->bindValue(':text', $text, SQLITE3_TEXT);
$statement->bindValue(':timestamp', $timestamp, SQLITE3_INTEGER);
$statement->bindValue(':room_id', $room_id, SQLITE3_INTEGER);

$statement->execute();

?>