<?php

include_once('../constants.php');

$action = $_GET['action'];

/*
$i = 1;
while($i <= 500) {
    $name = substr(md5(rand()), 0, 7);
    $text = md5(rand());
    $room = rand(1,2);
    $timestamp = time();
    echo 'INSERT INTO messages (name, text, room_id, timestamp) VALUES ("'.$name.'", "'.$text.'", '.$room.', "'.$timestamp.'");<br>';
    $i++;
}

exit;
*/

if($_GET['action'] && file_exists($_GET['action'].'.php')) {
    include_once(BASE_PATH . '/db.php');
    include_once(BASE_PATH . '/api/' . $_GET['action'].'.php');
} else {
    http_response_code(500);
    exit;
}

?>