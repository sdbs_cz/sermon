CREATE TABLE `messages`
(
    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
    `room_id`   INTEGER,
    `name`      TEXT,
    `text`      TEXT,
    `timestamp` INTEGER
);

CREATE TABLE `rooms`
(
    `id`        INTEGER PRIMARY KEY AUTOINCREMENT,
    `name`      TEXT
);

INSERT INTO `rooms` (`name`) VALUES (`default`)
